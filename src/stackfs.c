/* Copyright (C) 2013 Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * Refer to COPYING for the license of this program.
 * 
 */
#include <config.h>

#define	_XOPEN_SOURCE	700

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/vfs.h>
#include <unistd.h>

#define FUSE_USE_VERSION		26
#include <fuse.h>

#include "stackfs.h"

/**
 * stackfs modules
 */

int stackfs_n_mdbs;
struct stackfs_metadb *registered_mdbs[STACKFS_MAX_MODULES];

int stackfs_n_filers;
struct stackfs_filer *registered_filers[STACKFS_MAX_MODULES];

static struct stackfs_ctx *__ctx;

/**
 * stackfs implementation of fuse operations.
 */

#define	stackfs_fuse_ctx			\
		((struct stackfs_ctx *) (fuse_get_context()->private_data))

static int stackfs_getattr(const char *path, struct stat *stbuf)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->getattr(ctx, path, stbuf);
}

static int stackfs_readlink(const char *path, char *link, size_t size)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->readlink(ctx, path, link, size);
}

static int stackfs_mknod(const char *path, mode_t mode, dev_t dev)
{
	int ret;
	uint64_t ino;
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	ret = stackfs_mdb_ops(ctx)->mknod(ctx, path, mode, dev, &ino);
	if (ret)
		return ret;

	if (S_ISREG(mode))	/** create a backend for a regular file */
		ret = stackfs_filer_ops(ctx)->create(ctx, ino);

	return ret;
}

static int stackfs_mkdir(const char *path, mode_t mode)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->mkdir(ctx, path, mode);
}

static int stackfs_unlink(const char *path)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->unlink(ctx, path);
}

static int stackfs_rmdir(const char *path)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->rmdir(ctx, path);
}

static int stackfs_symlink(const char *path, const char *link)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->symlink(ctx, path, link);
}

static int stackfs_rename(const char *old, const char *new)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->rename(ctx, old, new);
}

static int stackfs_link(const char *path, const char *new)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->link(ctx, path, new);
}

static int stackfs_chmod(const char *path, mode_t mode)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->chmod(ctx, path, mode);
}

static int stackfs_chown(const char *path, uid_t uid, gid_t gid)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->chown(ctx, path, uid, gid);
}

static int stackfs_truncate(const char *path, off_t newsize)
{
	int ret;
	uint64_t ino;
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	stackfs_mdb_ops(ctx)->tx_begin(ctx);

	ret = stackfs_mdb_ops(ctx)->truncate(ctx, path, newsize);
	if (ret)
		goto out_fail;

	ret = stackfs_mdb_ops(ctx)->get_ino(ctx, path, &ino);
	if (ret)
		goto out_fail;
	ret = stackfs_filer_ops(ctx)->truncate(ctx, ino, newsize);
	if (ret)
		goto out_fail;

	stackfs_mdb_ops(ctx)->tx_commit(ctx);
	return 0;

out_fail:
	stackfs_mdb_ops(ctx)->tx_abort(ctx);
	return ret;
}

static int stackfs_utime(const char *path, struct utimbuf *tbuf)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->utime(ctx, path, tbuf);
}

static int stackfs_open(const char *path, struct fuse_file_info *fi)
{
	int ret;
	uint64_t ino;
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	ret = stackfs_mdb_ops(ctx)->get_ino(ctx, path, &ino);
	if (ret)
		return ret;
	ret = stackfs_filer_ops(ctx)->open(ctx, ino, fi->flags);
	if (ret < 0)
		return -errno;

	fi->fh = ret;
	return 0;
}

static int stackfs_read(const char *path, char *buf, size_t size,
			off_t offset, struct fuse_file_info *fi)
{
	stackfs_unused(path);

	return pread(fi->fh, buf, size, offset);
}

static int stackfs_write(const char *path, const char *buf, size_t size,
			off_t offset, struct fuse_file_info *fi)
{
	int ret;
	size_t written;
	struct stat stbuf;
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	written = pwrite(fi->fh, buf, size, offset);
	if (written < 0)
		goto out_fail;

	/**
	 * even though we use db transaction here, this is not reliable at all
	 * in case of any failures on metadata updates.
	 */

	stackfs_mdb_ops(ctx)->tx_begin(ctx);

	/**
	 * if file size has been changed (extended), update it.
	 */
	ret = stackfs_mdb_ops(ctx)->getattr(ctx, path, &stbuf);
	if (ret)
		goto out_fail;

	if (stbuf.st_size < offset + written) {
		ret = stackfs_mdb_ops(ctx)->truncate(ctx, path,
						offset + written);
		if (ret)
			goto out_fail;
	}

	stackfs_mdb_ops(ctx)->tx_commit(ctx);
	return written;

out_fail:
	stackfs_mdb_ops(ctx)->tx_abort(ctx);
	return ret;
}

static int stackfs_statfs(const char *path, struct statvfs *vfs)
{
#if 0
	vfs->f_bsize = 4096;
	vfs->f_blocks = (10*(1<<20));
	vfs->f_bfree = (10*(1<<20));
	vfs->f_bavail = (10*(1<<20));
	vfs->f_files = (10*(1<<20));
	vfs->f_ffree = (10*(1<<20));
	vfs->f_namemax = STACKFS_NAMELEN;

	/** these fields are ignored. */
	vfs->f_frsize = 0;
	vfs->f_favail = 0;
	vfs->f_fsid = 0;
	vfs->f_flag = 0;
#endif
	int ret;
	struct statfs fs;

	if ((ret = statfs("/", &fs)) < 0)
		return ret;

	vfs->f_bsize = fs.f_bsize;
	vfs->f_frsize = 512;
	vfs->f_blocks = fs.f_blocks;
	vfs->f_bfree = fs.f_bfree;
	vfs->f_bavail = fs.f_bavail;
	vfs->f_files = 0xfffffff;
	vfs->f_ffree = 0xffffff;
	vfs->f_fsid = STACKFS_MAGIC;
	vfs->f_flag = 0;
	vfs->f_namemax = 256;

	return 0;
}

static int stackfs_flush(const char *path, struct fuse_file_info *fi)
{
	/** this is called upon each close() call */
	return 0;
}

static int stackfs_release(const char *path, struct fuse_file_info *fi)
{
	/** no more references to the file handle. */
	return close(fi->fh);
}

static int stackfs_fsync(const char *path, int datasync,
			struct fuse_file_info *fi)
{
	return datasync ? fdatasync(fi->fh) : fsync(fi->fh);
}

/**
 * supporting extended attributes is optional.
 */

static int stackfs_setxattr(const char *path, const char *name,
			const char *value, size_t size, int flags)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	if (!stackfs_mdb_ops(ctx)->setxattr)
		return -ENOSYS;
	return stackfs_mdb_ops(ctx)->setxattr(ctx, path, name, value, size,
						flags);
}

static int stackfs_getxattr(const char *path, const char *name,
			char *value, size_t size)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	if (!stackfs_mdb_ops(ctx)->getxattr)
		return -ENOSYS;
	return stackfs_mdb_ops(ctx)->getxattr(ctx, path, name, value, size);
}

static int stackfs_listxattr(const char *path, char *list, size_t size)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	if (!stackfs_mdb_ops(ctx)->listxattr)
		return -ENOSYS;
	return stackfs_mdb_ops(ctx)->listxattr(ctx, path, list, size);
}

static int stackfs_removexattr(const char *path, const char *name)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	if (!stackfs_mdb_ops(ctx)->removexattr)
		return -ENOSYS;
	return stackfs_mdb_ops(ctx)->removexattr(ctx, path, name);
}

/**
 * our implementation of readdir is stateless.
 */

static int stackfs_opendir(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static int stackfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			off_t offset, struct fuse_file_info *fi)
{
	int ret;
	uint64_t pos = offset;
	struct stackfs_dirent dirent;
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	/** TODO:
	 * currently, we read entries one by one, which might harm performance.
	 * batch this.
	 */

	do {
		ret = stackfs_mdb_ops(ctx)->readdir(ctx, path, pos, &dirent);
		if (ret == 0)
			break;		/** finished */
		else if (ret < 0)
			return ret;	/** failed */
		else {
			ret = filler(buf, dirent.d_name, &dirent.stat, 0);
			pos++;
		}
	} while (ret == 0);

	/** filler returns 1 on running out of its buf */
	return 0;
}

static int stackfs_releasedir(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static int stackfs_fsyncdir(const char *path, int datasync,
				struct fuse_file_info *fi)
{
	return 0;
}

static void *stackfs_init(struct fuse_conn_info *conn)
{
	int ret;

	stackfs_unused(conn);

	/**
	 * initialize submodules.
	 */
	if (__ctx->mdb->ops->init) {
		ret = __ctx->mdb->ops->init(__ctx);
		if (ret) {
			fprintf(stderr, "init mdb failed!\n");
			goto out_err;
		}
	}
	if (__ctx->filer->ops->init) {
		ret = __ctx->filer->ops->init(__ctx);
		if (ret) {
			fprintf(stderr, "init filer failed!\n");
			goto out_err;
		}
	}

	return __ctx;

out_err:
	exit(1);
}

static void stackfs_destroy(void *context)
{
	struct stackfs_ctx *ctx = (struct stackfs_ctx *) context;

	if (stackfs_mdb_ops(ctx)->exit)
		stackfs_mdb_ops(ctx)->exit(ctx);
	if (stackfs_filer_ops(ctx)->exit)
		stackfs_filer_ops(ctx)->exit(ctx);
}

static int stackfs_access(const char *path, int mask)
{
	/** TODO: implement this */
	return 0;
}

static int stackfs_ftruncate(const char *path, off_t newsize,
				struct fuse_file_info *fi)
{
	int ret = 0;
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;

	stackfs_mdb_ops(ctx)->tx_begin(ctx);

	ret = stackfs_mdb_ops(ctx)->truncate(ctx, path, newsize);
	if (ret)
		goto out_fail;

	/** fd is already with us, no need to call filer. */
	ret = ftruncate(fi->fh, newsize);
	if (ret)
		goto out_fail;

	stackfs_mdb_ops(ctx)->tx_commit(ctx);

	return ret;

out_fail:
	stackfs_mdb_ops(ctx)->tx_abort(ctx);
	return ret;
}

static int stackfs_fgetattr(const char *path, struct stat *stbuf,
			struct fuse_file_info *fi)
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->getattr(ctx, path, stbuf);
}

#if 0
static int stackfs_utimens(const char *path, const struct timespec tv[2])
{
	struct stackfs_ctx *ctx = stackfs_fuse_ctx;
	return stackfs_mdb_ops(ctx)->utimens(ctx, path, tv);
}
#endif

static struct fuse_operations stackfs_ops = {
	.getattr	= stackfs_getattr,
	.readlink	= stackfs_readlink,
	.getdir		= NULL,
	.mknod		= stackfs_mknod,
	.mkdir		= stackfs_mkdir,
	.unlink		= stackfs_unlink,
	.rmdir		= stackfs_rmdir,
	.symlink	= stackfs_symlink,
	.rename		= stackfs_rename,
	.link		= stackfs_link,
	.chmod		= stackfs_chmod,
	.chown		= stackfs_chown,
	.truncate	= stackfs_truncate,
	.utime		= stackfs_utime,
	.open		= stackfs_open,
	.read		= stackfs_read,
	.write		= stackfs_write,
	.statfs		= stackfs_statfs,
	.flush		= stackfs_flush,
	.release	= stackfs_release,
	.fsync		= stackfs_fsync,
	.setxattr	= stackfs_setxattr,
	.getxattr	= stackfs_getxattr,
	.listxattr	= stackfs_listxattr,
	.removexattr	= stackfs_removexattr,
	.opendir	= stackfs_opendir,
	.readdir	= stackfs_readdir,
	.releasedir	= stackfs_releasedir,
	.fsyncdir	= stackfs_fsyncdir,
	.init		= stackfs_init,
	.destroy	= stackfs_destroy,
	.access		= stackfs_access,
	.create		= NULL,
	.ftruncate	= stackfs_ftruncate,
	.fgetattr	= stackfs_fgetattr,
	.lock		= NULL,
#if 0
	/** currently, stick to deprecated utime(2) */
	.utimens	= stackfs_utimens,
#endif
	.bmap		= NULL,
};

/**
 * main program
 */

/**
 * TODO: option parsing, will finish later.
 */
enum {
	OPTKEY_DEBUG	= 0,
	OPTKEY_MDB,
	OPTKEY_FILER,
	OPTKEY_HELP
};

static struct fuse_opt stackfs_fuse_opts[] = {
	FUSE_OPT_KEY("-d", OPTKEY_DEBUG),
	FUSE_OPT_KEY("--debug", OPTKEY_DEBUG),
	FUSE_OPT_KEY("--mdb=%s", OPTKEY_MDB),
	FUSE_OPT_KEY("-m %s", OPTKEY_MDB),
	FUSE_OPT_KEY("--filer=%s", OPTKEY_FILER),
	FUSE_OPT_KEY("-f %s", OPTKEY_FILER),
	FUSE_OPT_KEY("--help", OPTKEY_HELP),
	FUSE_OPT_KEY("-h", OPTKEY_HELP),
	FUSE_OPT_END
};

static int stackfs_debug;
static const char *stackfs_root;
static const char *mdb_name;
static const char *filer_name;

static int stackfs_set_module(struct stackfs_ctx *ctx)
{
	int i, ret = 0;
	struct stackfs_metadb *mdb;
	struct stackfs_filer *filer;

	if (!mdb_name)
		mdb_name = STACKFS_DEFAULT_MDB_NAME;
	if (!filer_name)
		filer_name = STACKFS_DEFAULT_FILER_NAME;

	ctx->mdb = NULL;
	ctx->filer = NULL;

	for (i = 0; i < stackfs_n_mdbs; i++) {
		mdb = registered_mdbs[i];
		if (!strcmp(mdb_name, mdb->name)) {
			ctx->mdb = mdb;
			break;
		}
	}

	for (i = 0; i < stackfs_n_filers; i++) {
		filer = registered_filers[i];
		if (!strcmp(filer_name, filer->name)) {
			ctx->filer = filer;
			break;
		}
	}

	if (!ctx->mdb || !ctx->filer)
		ret = -EINVAL;

	return ret;
}

static inline char *arg_parse_strval(const char *arg)
{
	if (arg[1] == '-')
		return strdup(&arg[2]);
	else
		return strdup(strchr(arg, '='));
}

static void usage(void)
{
	printf(
	"Usage: stackfs [options] mountpoint\n"
	"\n"
	"options:\n"
	"    -d                  Enable debug output\n"
	"    -o opt,[opt...]     mount options\n"
	"    -h, --help          print help\n"
	"    -m, --mdb=<name>    metadata module name (default: sqlite)\n"
	"    -f, --filer=<filer> filer module name (default: naive)\n\n");
}

static int stackfs_process_opt(void *data, const char *arg, int key,
				struct fuse_args *outargs)
{
	switch (key) {
	case OPTKEY_DEBUG:
		stackfs_debug = 1;
		return 1;
	case OPTKEY_MDB:
		mdb_name = arg_parse_strval(arg);
		break;
	case OPTKEY_FILER:
		filer_name = arg_parse_strval(arg);
		break;
	case OPTKEY_HELP:
		usage();
		exit(1);
	default:
		if (arg[0] != '-') {
			/** this is a mount point */
			stackfs_root = strdup(arg);
		}
		return 1;
	}

	return 0;
}

int main(int argc, char **argv)
{
	int ret;
	int uid, gid;
	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

	if (fuse_opt_parse(&args, NULL, stackfs_fuse_opts,
				stackfs_process_opt) < 0)
	{
		return -EINVAL;
	}

	uid = getuid();
	gid = getgid();

#ifdef FUSE_CAP_BIG_WRITES
	if (fuse_opt_add_arg(&args, "-obig_writes")) {
		fprintf(stderr, "failed to enable big writes!\n");
		return -EINVAL;
	}
#endif

	__ctx = calloc(1, sizeof(*__ctx));
	if (!__ctx)
		return -ENOMEM;

	__ctx->root = stackfs_root;
	ret = stackfs_set_module(__ctx);
	if (ret < 0)
		return -EINVAL;

	return fuse_main(args.argc, args.argv, &stackfs_ops, __ctx);
}

