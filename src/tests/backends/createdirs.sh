#!/bin/bash

for i in `seq 1 3`; do
	dir="store$i"
	for n in `seq 0 $((0xff))`; do
		name="`printf %02x $n`"
		path="$dir/$name"
		mkdir -p $path
	done
done

