#ifndef	__STACKFS__
#define	__STACKFS__

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <utime.h>
#include <linux/limits.h>

struct stackfs_ctx;

/**
 * stackfs in-memory superblock.
 */

#define	STACKFS_MAGIC	0x7374636b	/** stck */

#if 0
/** currently, not necessary. */
struct stackfs_super {
	uint32_t	s_magic;
	uint64_t	s_ctime;
	uint64_t	s_mtime;
	uint64_t	s_mntcnt;
};
#endif


/**
 * stackfs in-memory dirent.
 */
#define	STACKFS_NAMELEN		256

struct stackfs_dirent {
	uint64_t	d_ino;
	char		d_name[STACKFS_NAMELEN];
	void		*d_priv;	/** anything you want */
	struct stat	stat;
};

/**
 * interface for different metadata backend implementations. private pointer
 * can be obtained using stackfs_mdb_private(ctx) macro.
 *
 * @init: module initialization.
 * @exit: module cleanup.
 * @tx_begin: transaction begin, for atomic operations.
 * @tx_commit: transaction commit, for atomic operations.
 * @tx_abort: transaction abort, for atomic operations.
 * @get_ino: get inode # from path.
 *
 * refer to fuse.h for documentation of rest of the fields.
 */
struct stackfs_metadb_operations {
	int (*init) (struct stackfs_ctx *ctx);
	void (*exit) (struct stackfs_ctx *ctx);

	int (*tx_begin) (struct stackfs_ctx *ctx);
	int (*tx_commit) (struct stackfs_ctx *ctx);
	int (*tx_abort) (struct stackfs_ctx *ctx);

#define	STACKFS_INO_INVALID	-1UL
	int (*get_ino) (struct stackfs_ctx *ctx, const char *path,
			uint64_t *ino);

	int (*getattr) (struct stackfs_ctx *ctx, const char *path,
			struct stat *stbuf);
	int (*readlink) (struct stackfs_ctx *ctx, const char *path, char *link,
			size_t size);
	int (*mknod) (struct stackfs_ctx *ctx, const char *path, mode_t mode,
			dev_t dev, uint64_t *ino);
	int (*mkdir) (struct stackfs_ctx *ctx, const char *path, mode_t mode);
	int (*unlink) (struct stackfs_ctx *ctx, const char *path);
	int (*rmdir) (struct stackfs_ctx *ctx, const char *path);
	int (*symlink) (struct stackfs_ctx *ctx, const char *path,
			const char *link);
	int (*rename) (struct stackfs_ctx *ctx, const char *old,
			const char *new);
	int (*link) (struct stackfs_ctx *ctx, const char *path,
			const char *new);
	int (*chmod) (struct stackfs_ctx *ctx, const char *path, mode_t mode);
	int (*chown) (struct stackfs_ctx *ctx, const char *path, uid_t uid,
			gid_t gid);
	int (*truncate) (struct stackfs_ctx *ctx, const char *path,
			off_t newsize);
	int (*utime) (struct stackfs_ctx *ctx, const char *path,
			struct utimbuf *tbuf);
	int (*readdir) (struct stackfs_ctx *ctx, const char *path, off_t pos,
			struct stackfs_dirent *dirent);

	/**
	 * functions below are optional.
	 */
	int (*setxattr) (struct stackfs_ctx *ctx, const char *path,
			const char *name, const char *value, size_t size,
			int flags);
	int (*getxattr) (struct stackfs_ctx *ctx, const char *path,
			const char *name, char *value, size_t size);
	int (*listxattr) (struct stackfs_ctx *ctx, const char *path,
			char *list, size_t size);
	int (*removexattr) (struct stackfs_ctx *ctx, const char *path,
			const char *name);
	int (*utimens) (struct stackfs_ctx *ctx, const char *path,
			const struct timespec tv[2]);
};

struct stackfs_metadb {
	const char *name;
	struct stackfs_metadb_operations *ops;
	struct stackfs_ctx *ctx;
	void *priv;
};

/**
 * interface for different filer implementations. private pointer can be
 * obtained from stackfs_filer_private(ctx) macro.
 *
 * @init: module initialization.
 * @exit: module cleanup.
 * @open: returns the file descriptor according to @ino.
 * @truncate: adjust underlying filesize if necessary.
 */
struct stackfs_filer_operations {
	int (*init) (struct stackfs_ctx *ctx);
	void (*exit) (struct stackfs_ctx *ctx);

	int (*open) (struct stackfs_ctx *ctx, uint64_t ino, int flags);
	int (*create) (struct stackfs_ctx *ctx, uint64_t ino);
	int (*truncate) (struct stackfs_ctx *ctx, uint64_t ino,
			uint64_t newsize);
};

struct stackfs_filer {
	const char *name;
	struct stackfs_filer_operations *ops;
	struct stackfs_ctx *ctx;
	void *priv;
};

/**
 * stackfs runtime context.
 */
struct stackfs_ctx {
	const char *root;
	struct stackfs_metadb *mdb;
	struct stackfs_filer *filer;
};

#define	stackfs_realpath(ctx)		((ctx)->root)

/**
 * buf should be enough to hold PATHMAX
 */
static inline
char *stackfs_fullpath(struct stackfs_ctx *ctx, const char *path, char *buf)
{
	strcpy(buf, stackfs_realpath(ctx));
	strncat(buf, path, PATH_MAX);
	return buf;
}

#define	stackfs_mbd(ctx)		((ctx)->mdb)
#define	stackfs_filer(ctx)		((ctx)->filer)
#define	stackfs_mdb_ops(ctx)		((ctx)->mdb->ops)
#define	stackfs_filer_ops(ctx)		((ctx)->filer->ops)
#define stackfs_mdb_private(ctx)	((ctx)->mdb->priv)
#define	stackfs_filer_private(ctx)	((ctx)->filer->priv)

#define STACKFS_DEFAULT_MDB_NAME	"sqlite"
#define	STACKFS_DEFAULT_FILER_NAME	"rr"

#define	STACKFS_MAX_MODULES		8

/**
 * register new modules. defined in stackfs.c
 */
extern int stackfs_n_mdbs;
extern int stackfs_n_filers;
extern struct stackfs_metadb *registered_mdbs[STACKFS_MAX_MODULES];
extern struct stackfs_filer *registered_filers[STACKFS_MAX_MODULES];

static inline int stackfs_register_metadb(struct stackfs_metadb *mdb)
{
	if (!mdb)
		return -EINVAL;
	if (stackfs_n_mdbs == STACKFS_MAX_MODULES)
		return -ENOMEM;

	registered_mdbs[stackfs_n_mdbs++] = mdb;
	return 0;
}

static inline int stackfs_register_filer(struct stackfs_filer *filer)
{
	if (!filer)
		return -EINVAL;
	if (stackfs_n_filers == STACKFS_MAX_MODULES)
		return -ENOMEM;

	registered_filers[stackfs_n_filers++] = filer;
	return 0;
}

#include "utils.h"

#endif	/** __STACKFS__ */

