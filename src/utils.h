#ifndef	__UTILS_H__
#define	__UTILS_H__

/**
 * frequently used macros
 */

#ifndef	stackfs_max
#define	stackfs_max(x, y)	((x) > (y) ? (x) : (y))
#endif

#ifndef	stackfs_min
#define	stackfs_min(x, y)	((x) < (y) ? (x) : (y))
#endif

#ifndef	stackfs_llu
#define	stackfs_llu(x)		((unsigned long long) (x))
#endif

#ifndef	stackfs_unused
#define	stackfs_unused(x)	((void) (x))
#endif

#include <ctype.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>

/**
 * getting timestamp of current time
 */

static inline uint64_t stackfs_now_sec(void)
{
	struct timeval tmp;
	gettimeofday(&tmp, NULL);
	return tmp.tv_sec;
}

static inline uint64_t stackfs_noe_usec(void)
{
	struct timeval tmp;
	gettimeofday(&tmp, NULL);
	return tmp.tv_sec * 1000000 + tmp.tv_usec;
}

/**
 * some string utilities
 */

/**
 * strtrim implementation.
 *
 * note that for trimming operation (especially for ltrim), the caller lose
 * the pointer to the original string.
 * also be warned that this was not tested fully.
 *
 * taken from:
 * http://stackoverflow.com/questions/656542/trim-a-string-in-c
 * written by JRL
 */

static inline char *strrtrim(char *s)
{
	while (isspace(*s)) s++;
	return s;
}

static inline char *strltrim(char *s)
{
	char *back = &s[strlen(s)];
	while (isspace(*--back));
	back[1] = '\0';
	return s;
}

static inline char *strtrim(char *str)
{
	return strrtrim(strltrim(str));
}

static inline int strempty(char *str)
{
	if (strlen(str) == 0)
		return 1;

	while (*str)
		if (!isspace(*str++))
			return 0;
	return 1;
}

#endif	/** __UTILS_H__ */

