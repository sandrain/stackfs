/* Copyright (C) 2013 Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * Refer to COPYING for the license of this program.
 * 
 */
#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sqlite3.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "stackfs.h"

enum {
	MDB_SQL_FINDINO	= 0,
	MDB_SQL_INODEAPPEND,		/* 01 */
	MDB_SQL_DIRENTAPPEND,		/* 02 */
	MDB_SQL_DIRENTREMOVE,		/* 03 */
	MDB_SQL_SYMLINKAPPEND,		/* 04 */
	MDB_SQL_GETATTR,		/* 05 */
	MDB_SQL_READLINK,		/* 06 */
	MDB_SQL_CHMOD,			/* 07 */
	MDB_SQL_CHOWN,			/* 08 */
	MDB_SQL_TRUNCATE,		/* 09 */
	MDB_SQL_UTIME,			/* 10 */
	MDB_SQL_READDIR,		/* 11 */

	N_MDB_SQLS
};

struct mdb_sqlite_ctx {
	const char *dbpath;
	sqlite3 *conn;

	sqlite3_stmt *stmts[N_MDB_SQLS];
};

/**
 * TODO: make a parameter for this.
 */
static const char *dbpath = "/tmp/stackfs/backends/sqlite.db";

/**
 * sqls 
 */
static const char *mdb_sqls[N_MDB_SQLS] = {
	/** 00. MDB_SQL_FINDINO */
	"select e_ino from stfs_dirent where d_ino=? and name=?",

	/** 01. MDB_SQL_INODEAPPEND */
	"insert into stfs_inode "
	"(dev,mode,nlink,uid,gid,rdev,size,atime,mtime,ctime) values "
	"(?,?,?,?,?,?,?,?,?,?)",

	/** 02. MDB_SQL_DIRENTAPPEND */
	"insert into stfs_dirent (d_ino,e_ino,name) values (?,?,?)",

	/** 03. MDB_SQL_DIRENTREMOVE */
	"delete from stfs_dirent where d_ino=? and name=?",

	/** 04. MDB_SQL_SYMLINKAPPEND */
	"insert into stfs_symlink (ino,path) values (?,?)",

	/** 05. MDB_SQL_GETATTR */
	"select * from stfs_inode where id=?",

	/** 06. MDB_SQL_READLINK */
	"select path from stfs_symlink where ino=?",

	/** 07. MDB_SQL_CHMOD */
	"update stfs_inode set mode=? where id=?",

	/** 08. MDB_SQL_CHOWN */
	"update stfs_inode set uid=?,gid=? where id=?",

	/** 09. MDB_SQL_TRUNCATE */
	"update stfs_inode set size=? where id=?",

	/** 10. MDB_SQL_UTIME */
	"update stfs_inode set atime=?,mtime=? where id=?",

	/** 11. MDB_SQL_READDIR */
	/** limit <skip>,<count> */
	"select e_ino,name from stfs_dirent where d_ino=? limit ?,?"
};

/**
 * helper functions.
 */
#define	sqlite_ctx(ctx)				\
		((struct mdb_sqlite_ctx *) stackfs_mdb_private(ctx))

static inline int exec_simple_sql(struct mdb_sqlite_ctx *self,
				const char *sql)
{
	int ret;
	ret = sqlite3_exec(self->conn, sql, NULL, NULL, NULL);
	return ret == SQLITE_OK ? 0 : ret;
}

static inline int set_pragma(struct mdb_sqlite_ctx *self, int safe)
{
	if (safe)
		return exec_simple_sql(self, "PRAGMA synchronous = OFF;");
	else
		return exec_simple_sql(self, "PRAGMA synchronous = OFF; "
				      "PRAGMA temp_store = 2;");
}

static inline sqlite3_stmt *stmt_get(struct mdb_sqlite_ctx *self, int id)
{
	return self->stmts[id];
}

static int validate_mode(mode_t mode)
{
	int ret = 0;
	unsigned int ifmt = mode & S_IFMT;

	if (S_ISCHR(mode) || S_ISBLK(mode)
		|| S_ISFIFO(mode) || S_ISSOCK(mode))
	{
		return EINVAL;
	}

	if (S_ISREG(mode)) {
		if (ifmt & ~S_IFREG)
			ret = EINVAL;
	}
	else if (S_ISDIR(mode)) {
		if (ifmt & ~S_IFDIR)
			ret = EINVAL;
	}

	return ret;

#if 0
	else if (S_ISCHR(mode)) {
		ret = EINVAL;
#if 0
		if (ifmt & ~S_IFCHR)
			ret = EINVAL;

#endif
	}
	else if (S_ISBLK(mode)) {
		ret = EINVAL;
#if 0
		if (ifmt & ~S_IFBLK)
			ret = EINVAL;
#endif
	}
	else if (S_ISFIFO(mode)) {
		ret = EINVAL;
#if 0
		if (ifmt & ~S_IFIFO)
			ret = EINVAL;
#endif
	}
	else if (S_ISLNK(mode)) {
		ret = 0;
	}
	else if (S_ISSOCK(mode)) {
		ret = EINVAL;
#if 0
		if (ifmt & ~S_IFSOCK)
			return EINVAL;
#endif
	}
	else
		ret = EINVAL;

	return ret;
#endif
}

static int lookup_dirent(struct mdb_sqlite_ctx *self, uint64_t parent,
			const char *name, uint64_t *ino_out)
{
	int ret;
	sqlite3_stmt *stmt;

	stmt = stmt_get(self, MDB_SQL_FINDINO);
	ret = sqlite3_bind_int64(stmt, 1, parent);
	ret |= sqlite3_bind_text(stmt, 2, name, -1, SQLITE_STATIC);
	if (ret) {
		ret = -EIO;
		goto out;
	}

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_ROW) {
		ret = ret == SQLITE_DONE ? -ENOENT : -EIO;
		goto out;
	}

	*ino_out = sqlite3_column_int64(stmt, 0);
	ret = 0;
out:
	sqlite3_reset(stmt);
	return ret;
}

static int find_ino_from_path(struct mdb_sqlite_ctx *self,
				const char *path, uint64_t *ino_out)
{
	int ret;
	uint64_t ino, parent;
	char *sp;
	char *buf = strdup(path);
	size_t len = strlen(buf);

	if (len > 1 && buf[len - 1] == '/')
		buf[len - 1] = '\0';

	parent = 1;
	while ((sp = strsep(&buf, "/")) != NULL) {
		if (strempty(sp))
			continue;

		ret = lookup_dirent(self, parent, sp, &ino);
		if (ret)
			goto out;

		parent = ino;
	}

	*ino_out = parent;
	ret = 0;
out:
	free(buf);
	return ret;
}

static inline int is_root_path(const char *path)
{
	const char *pos = path;

	while (*pos)
		if (*pos++ != '/')
			return 0;

	return 1;
}

/**
 * TODO; check if path can contain consecutive '/'s.
 * e.g. "///" for root
 */

/**
 *the memory space should be deallocated by the caller.
 */
static inline char *parent_path(const char *path)
{
	char *pos, *buf;

	if (is_root_path(path))
		return (char *) path;

	buf = strdup(path);
	pos = strrchr(buf, '/');
	pos[0] = '\0';

	return buf;
}

static inline const char *filename(const char *path)
{
	const char *pos;

	if (is_root_path(path))
		return path;

	pos = strrchr(path, '/');
	return &pos[1];
}

static int find_parent_ino_from_path(struct mdb_sqlite_ctx *self,
				const char *path, uint64_t *ino_out)
{
	int ret;
	char *parent = parent_path(path);

	ret = find_ino_from_path(self, parent, ino_out);
	free(parent);

	return ret;
}

static int inode_append(struct mdb_sqlite_ctx *self, struct stat *stbuf,
			uint64_t *ino_out)
{
	int ret;
	sqlite3_stmt *stmt;

	stmt = stmt_get(self, MDB_SQL_INODEAPPEND);
	ret = sqlite3_bind_int(stmt, 1, stbuf->st_dev);
	ret |= sqlite3_bind_int(stmt, 2, stbuf->st_mode);
	ret |= sqlite3_bind_int(stmt, 3, stbuf->st_nlink);
	ret |= sqlite3_bind_int(stmt, 4, stbuf->st_uid);
	ret |= sqlite3_bind_int(stmt, 5, stbuf->st_gid);
	ret |= sqlite3_bind_int(stmt, 6, stbuf->st_rdev);
	ret |= sqlite3_bind_int64(stmt, 7, stbuf->st_size);
	ret |= sqlite3_bind_int64(stmt, 8, stbuf->st_atime);
	ret |= sqlite3_bind_int64(stmt, 9, stbuf->st_mtime);
	ret |= sqlite3_bind_int64(stmt, 10, stbuf->st_ctime);
	if (ret) {
		ret = -EIO;
		goto out;
	}

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret == SQLITE_DONE) {
		*ino_out = sqlite3_last_insert_rowid(self->conn);
		ret = 0;
	}
	else
		ret = -EIO;

out:
	sqlite3_reset(stmt);
	return ret;
}

/**
 * this is taken care of by sql trigger.
 */
#if 0
static int inode_remove(struct mdb_sqlite_ctx *self, uint64_t ino)
{
	return 0;
}
#endif

static int dirent_append(struct mdb_sqlite_ctx *self, uint64_t pino,
			uint64_t ino, const char *name, int isdir)
{
	int ret;
	sqlite3_stmt *stmt;

	stmt = stmt_get(self, MDB_SQL_DIRENTAPPEND);
	ret = sqlite3_bind_int64(stmt, 1, pino);
	ret |= sqlite3_bind_int64(stmt, 2, ino);
	ret |= sqlite3_bind_text(stmt, 3, name, -1, SQLITE_STATIC);
	if (ret) {
		ret = -EIO;
		goto out;
	}

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_DONE) {
		ret = -EIO;
		goto out;
	}

	ret = 0;
	if (!isdir)
		goto out;

	/**
	 * create . and .. entries.
	 */
	sqlite3_reset(stmt);
	ret = sqlite3_bind_int64(stmt, 1, ino);
	ret |= sqlite3_bind_int64(stmt, 2, ino);
	ret |= sqlite3_bind_text(stmt, 3, ".", -1, SQLITE_STATIC);

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_DONE) {
		ret = -EIO;
		goto out;
	}

	ret = 0;
	sqlite3_reset(stmt);
	ret = sqlite3_bind_int64(stmt, 1, ino);
	ret |= sqlite3_bind_int64(stmt, 2, pino);
	ret |= sqlite3_bind_text(stmt, 3, "..", -1, SQLITE_STATIC);

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_DONE) {
		ret = -EIO;
		goto out;
	}

	ret = 0;
out:
	sqlite3_reset(stmt);
	return ret;
}

static int dirent_remove(struct mdb_sqlite_ctx *self, const uint64_t pino,
			const char *name)
{
	int ret;
	sqlite3_stmt *stmt;

	stmt = stmt_get(self, MDB_SQL_DIRENTREMOVE);
	ret = sqlite3_bind_int64(stmt, 1, pino);
	ret |= sqlite3_bind_text(stmt, 2, name, -1, SQLITE_STATIC);
	if (ret) {
		ret = -EIO;
		goto out;
	}

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_DONE) {
		ret = -EIO;
		goto out;
	}

	if (sqlite3_changes(self->conn) == 0) {
		ret = -ENOENT;
		goto out;
	}

	ret = 0;
out:
	sqlite3_reset(stmt);
	return ret;
}

static int symlink_append(struct mdb_sqlite_ctx *self, uint64_t ino,
			const char *path)
{
	int ret;
	sqlite3_stmt *stmt;

	stmt = stmt_get(self, MDB_SQL_SYMLINKAPPEND);
	ret = sqlite3_bind_int64(stmt, 1, ino);
	ret |= sqlite3_bind_text(stmt, 2, path, -1, SQLITE_STATIC);
	if (ret) {
		ret = -EIO;
		goto out;
	}

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	ret = ret == SQLITE_DONE ? 0 : -EIO;

out:
	sqlite3_reset(stmt);
	return ret;
}

static int get_inode_attr(struct mdb_sqlite_ctx *self, uint64_t ino,
			struct stat *stbuf)
{
	int ret;
	sqlite3_stmt *stmt;

	stmt = stmt_get(self, MDB_SQL_GETATTR);
	ret = sqlite3_bind_int64(stmt, 1, ino);
	if (ret) {
		ret = -EIO;
		goto out;
	}

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_ROW) {
		ret = ret == SQLITE_DONE ? -ENOENT : -EIO;
		goto out;
	}

	stbuf->st_ino = ino;
	stbuf->st_dev = sqlite3_column_int(stmt, 1);
	stbuf->st_mode = sqlite3_column_int(stmt, 2);
	stbuf->st_nlink = sqlite3_column_int(stmt, 3);
	stbuf->st_uid = sqlite3_column_int(stmt, 4);
	stbuf->st_gid = sqlite3_column_int(stmt, 5);
	stbuf->st_rdev = sqlite3_column_int(stmt, 6);
	stbuf->st_size = sqlite3_column_int64(stmt, 7);
	stbuf->st_blksize = 4096;
	stbuf->st_blocks = (stbuf->st_size >> 12) + 1;
	stbuf->st_atime = sqlite3_column_int64(stmt, 8);
	stbuf->st_ctime = sqlite3_column_int64(stmt, 9);
	stbuf->st_mtime = sqlite3_column_int64(stmt, 10);

	ret = 0;
out:
	sqlite3_reset(stmt);
	return ret;
}

/**
 * mdb_sqlite stackfs implementation.
 */

static int mdb_sqlite_tx_begin(struct stackfs_ctx *ctx)
{
	return exec_simple_sql(sqlite_ctx(ctx), "BEGIN TRANSACTION");
}

static int mdb_sqlite_tx_commit(struct stackfs_ctx *ctx)
{
	return exec_simple_sql(sqlite_ctx(ctx), "END TRANSACTION");
}

static int mdb_sqlite_tx_abort(struct stackfs_ctx *ctx)
{
	return exec_simple_sql(sqlite_ctx(ctx), "ROLLBACK");
}

static int mdb_sqlite_get_ino(struct stackfs_ctx *ctx, const char *path,
				uint64_t *ino)
{
	return find_ino_from_path(sqlite_ctx(ctx), path, ino);
}

static int mdb_sqlite_getattr(struct stackfs_ctx *ctx, const char *path,
				struct stat *stbuf)
{
	int ret;
	uint64_t ino = STACKFS_INO_INVALID;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_ino_from_path(self, path, &ino);
	if (ret)
		return ret;

	return get_inode_attr(self, ino, stbuf);
}

static int mdb_sqlite_readlink(struct stackfs_ctx *ctx, const char *path,
				char *link, size_t size)
{
	int ret;
	uint64_t ino = STACKFS_INO_INVALID;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);
	sqlite3_stmt *stmt;
	const char *tmp;

	ret = find_ino_from_path(self, path, &ino);
	if (ret)
		return ret;

	stmt = stmt_get(self, MDB_SQL_READLINK);
	ret = sqlite3_bind_int64(stmt, 1, ino);
	if (ret) {
		ret = -EIO;
		goto out;
	}

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_ROW) {
		ret = ret == SQLITE_DONE ? -ENOENT : -EIO;
		goto out;
	}

	tmp = (const char *) sqlite3_column_text(stmt, 0);
	if (strlen(tmp) <= size)
		strcpy(link, tmp);
	else {
		strncpy(link, tmp, size);
		link[size-1] = '\0';
	}
	ret = 0;

out:
	sqlite3_reset(stmt);
	return ret;
}

static int mdb_sqlite_mknod(struct stackfs_ctx *ctx, const char *path,
				mode_t mode, dev_t dev, uint64_t *ino_out)
{
	int ret;
	uint64_t ino, pino;
	struct stat statbuf;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = validate_mode(mode);
	if (ret) {
		ret = -EINVAL;
		goto out_err;
	}

	ret = find_parent_ino_from_path(self, path, &pino);
	if (ret)
		goto out_err;
	ret = get_inode_attr(self, pino, &statbuf);
	if (ret)
		goto out_err;

	statbuf.st_dev = dev;
	statbuf.st_mode = mode;
	statbuf.st_nlink = 1;
	statbuf.st_atime = statbuf.st_mtime = statbuf.st_ctime
			= stackfs_now_sec();

	if (S_ISDIR(mode))
		statbuf.st_size = 4096;
	else if (S_ISLNK(mode))
		statbuf.st_size = *ino_out;
	else
		statbuf.st_size = 0;

	mdb_sqlite_tx_begin(ctx);

	ret = inode_append(self, &statbuf, &ino);
	if (ret)
		goto out_rollback;

	ret = dirent_append(self, pino, ino, filename(path), S_ISDIR(mode));
	if (ret)
		goto out_rollback;

	mdb_sqlite_tx_commit(ctx);
	*ino_out = ino;
	return 0;

out_err:
	return ret;
out_rollback:
	mdb_sqlite_tx_abort(ctx);
	return -EIO;
}

static int mdb_sqlite_mkdir(struct stackfs_ctx *ctx, const char *path,
				mode_t mode)
{
	uint64_t ino;

	mode &= ALLPERMS;
	mode |= S_IFDIR;

	return mdb_sqlite_mknod(ctx, path, mode, 0, &ino);
}

static int mdb_sqlite_unlink(struct stackfs_ctx *ctx, const char *path)
{
	int ret;
	uint64_t pino;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_parent_ino_from_path(self, path, &pino);
	if (ret)
		return ret;

	return dirent_remove(self, pino, filename(path));
}

static int mdb_sqlite_rmdir(struct stackfs_ctx *ctx, const char *path)
{
	return mdb_sqlite_unlink(ctx, path);
}

static int mdb_sqlite_symlink(struct stackfs_ctx *ctx, const char *path,
				const char *link)
{
	int ret;
	uint64_t dino, pino; /** source/dest and dest parent ino */
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_parent_ino_from_path(self, link, &pino);
	if (ret)
		return ret;

	mdb_sqlite_tx_begin(ctx);

	/** dirty hack: pass the size info throught the output ino pointer */
	dino = strlen(path);
	ret = mdb_sqlite_mknod(ctx, link,
			S_IFLNK | S_IRWXU | S_IRWXG | S_IRWXO, 0, &dino);
	if (ret)
		goto out_rollback;

	ret = symlink_append(self, dino, path);
	if (ret)
		goto out_rollback;

	mdb_sqlite_tx_commit(ctx);
	return 0;

out_rollback:
	mdb_sqlite_tx_abort(ctx);
	return ret;
}

static int mdb_sqlite_rename(struct stackfs_ctx *ctx, const char *old,
				const char *new)
{
	int ret;
	uint64_t old_pino, new_pino, ino;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_parent_ino_from_path(self, old, &old_pino);
	if (ret)
		return ret;
	ret = find_parent_ino_from_path(self, new, &new_pino);
	if (ret)
		return ret;
	ret = find_ino_from_path(self, old, &ino);
	if (ret)
		return ret;

	mdb_sqlite_tx_begin(ctx);

	ret = mdb_sqlite_unlink(ctx, old);
	if (ret)
		goto out_rollback;

	ret = dirent_append(self, new_pino, ino, filename(new), 0);
	if (ret)
		goto out_rollback;

	mdb_sqlite_tx_commit(ctx);
	return 0;

out_rollback:
	mdb_sqlite_tx_abort(ctx);
	return ret;
}

#if 0
static int mdb_sqlite_link(struct stackfs_ctx *ctx, const char *path,
				const char *new)
{
}
#endif

static int mdb_sqlite_chmod(struct stackfs_ctx *ctx, const char *path,
				mode_t mode)
{
	int ret;
	uint64_t ino;
	sqlite3_stmt *stmt;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_ino_from_path(self, path, &ino);
	if (ret)
		return ret;

	stmt = stmt_get(self, MDB_SQL_CHMOD);
	ret = sqlite3_bind_int(stmt, 1, mode);
	ret |= sqlite3_bind_int64(stmt, 2, ino);
	if (ret)
		goto out;

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	ret = ret == SQLITE_DONE ? 0 : -EIO;

out:
	sqlite3_reset(stmt);
	return ret;
}

static int mdb_sqlite_chown(struct stackfs_ctx *ctx, const char *path,
				uid_t uid, gid_t gid)
{
	int ret;
	uint64_t ino;
	sqlite3_stmt *stmt;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_ino_from_path(self, path, &ino);
	if (ret)
		return ret;

	stmt = stmt_get(self, MDB_SQL_CHOWN);
	ret = sqlite3_bind_int(stmt, 1, uid);
	ret |= sqlite3_bind_int(stmt, 2, gid);
	ret |= sqlite3_bind_int64(stmt, 3, ino);
	if (ret)
		goto out;

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	ret = ret == SQLITE_DONE ? 0 : -EIO;

out:
	sqlite3_reset(stmt);
	return ret;
}

static int mdb_sqlite_truncate(struct stackfs_ctx *ctx, const char *path,
				off_t newsize)
{
	int ret;
	uint64_t ino;
	sqlite3_stmt *stmt;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_ino_from_path(self, path, &ino);
	if (ret)
		return ret;

	stmt = stmt_get(self, MDB_SQL_TRUNCATE);
	ret = sqlite3_bind_int64(stmt, 1, newsize);
	ret |= sqlite3_bind_int64(stmt, 2, ino);
	if (ret)
		goto out;

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	ret = ret == SQLITE_DONE ? 0 : -EIO;

out:
	sqlite3_reset(stmt);
	return ret;
}

static int mdb_sqlite_utime(struct stackfs_ctx *ctx, const char *path,
				struct utimbuf *tbuf)
{
	int ret;
	uint64_t ino;
	sqlite3_stmt *stmt;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_ino_from_path(self, path, &ino);
	if (ret)
		return ret;

	stmt = stmt_get(self, MDB_SQL_UTIME);
	ret = sqlite3_bind_int64(stmt, 1, tbuf->actime);
	ret |= sqlite3_bind_int64(stmt, 2, tbuf->modtime);
	ret |= sqlite3_bind_int64(stmt, 3, ino);
	if (ret)
		goto out;

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	ret = ret == SQLITE_DONE ? 0 : -EIO;

out:
	sqlite3_reset(stmt);
	return ret;
}

static int mdb_sqlite_readdir(struct stackfs_ctx *ctx, const char *path,
				off_t pos, struct stackfs_dirent *dirent)
{
	int ret;
	uint64_t dino;
	const char *tmp;
	sqlite3_stmt *stmt;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	ret = find_ino_from_path(self, path, &dino);
	if (ret)
		return ret;

	stmt = stmt_get(self, MDB_SQL_READDIR);
	ret = sqlite3_bind_int64(stmt, 1, dino);
	ret |= sqlite3_bind_int64(stmt, 2, pos);
	ret |= sqlite3_bind_int(stmt, 3, 1);
	if (ret)
		goto out;

	do {
		ret = sqlite3_step(stmt);
	} while (ret == SQLITE_BUSY);

	if (ret != SQLITE_ROW) {
		ret = ret == SQLITE_DONE ? 0 : -EIO;
		goto out;
	}

	dirent->d_ino = sqlite3_column_int64(stmt, 0);
	tmp = (const char *) sqlite3_column_text(stmt, 1);
	if (strlen(tmp) <= STACKFS_NAMELEN)
		strcpy(dirent->d_name, tmp);
	else {
		strncpy(dirent->d_name, tmp, STACKFS_NAMELEN);
		dirent->d_name[STACKFS_NAMELEN-1] = '\0';
	}
	ret = 1;

out:
	sqlite3_reset(stmt);
	return ret;
}

#if 0
static int mdb_sqlite_utimens(struct stackfs_ctx *ctx, const char *path,
				const struct timespec tv[2])
{
}

/**
 * functions below are optional.
 */
static int mdb_sqlite_setxattr(struct stackfs_ctx *ctx, const char *path,
				const char *name, const char *value,
				size_t size, int flags)
{
}

static int mdb_sqlite_getxattr(struct stackfs_ctx *ctx, const char *path,
				const char *name, char *value, size_t size)
{
}

static int mdb_sqlite_listxattr(struct stackfs_ctx *ctx, const char *path,
				char *list, size_t size)
{
}

static int mdb_sqlite_removexattr(struct stackfs_ctx *ctx, const char *path,
				const char *name)
{
}
#endif

static int mdb_sqlite_init(struct stackfs_ctx *ctx)
{
	int i, ret = 0;
	struct stackfs_metadb *mdb = ctx->mdb;
	struct mdb_sqlite_ctx *self = calloc(1, sizeof(*self));

	if (!self)
		return -ENOMEM;

	self->dbpath = dbpath;
	ret = sqlite3_open(dbpath, &self->conn);
	if (ret) {
		ret = -EIO;
		goto out_free;
	}

	for (i = 0; i < N_MDB_SQLS; i++) {
		ret = sqlite3_prepare(self->conn, mdb_sqls[i], -1,
					&self->stmts[i], NULL);
		if (ret != SQLITE_OK)
			goto out_close;
	}

	set_pragma(self, 0);

	mdb->ctx = ctx;
	mdb->priv = self;

	return ret;

out_close:
	sqlite3_close(self->conn);
out_free:
	free(self);
	return ret;
}

static void mdb_sqlite_exit(struct stackfs_ctx *ctx)
{
	int i;
	struct mdb_sqlite_ctx *self = sqlite_ctx(ctx);

	if (!self)
		return;

	for (i = 0; i < N_MDB_SQLS; i++)
		sqlite3_finalize(self->stmts[i]);

	sqlite3_close(self->conn);
	free(self);
}


static struct stackfs_metadb_operations mdb_sqlite_ops = {
	.init		= mdb_sqlite_init,
	.exit		= mdb_sqlite_exit,
	.tx_begin	= mdb_sqlite_tx_begin,
	.tx_commit	= mdb_sqlite_tx_commit,
	.tx_abort	= mdb_sqlite_tx_abort,
	.get_ino	= mdb_sqlite_get_ino,

	.getattr	= mdb_sqlite_getattr,
	.readlink	= mdb_sqlite_readlink,
	.mknod		= mdb_sqlite_mknod,
	.mkdir		= mdb_sqlite_mkdir,
	.unlink		= mdb_sqlite_unlink,
	.rmdir		= mdb_sqlite_rmdir,
	.symlink	= mdb_sqlite_symlink,
	.rename		= mdb_sqlite_rename,
#if 0
	.link		= mdb_sqlite_link, /** no support for hard link */
#endif
	.chmod		= mdb_sqlite_chmod,
	.chown		= mdb_sqlite_chown,
	.truncate	= mdb_sqlite_truncate,
	.utime		= mdb_sqlite_utime,
	.readdir	= mdb_sqlite_readdir,
#if 0
	.utimens	= mdb_sqlite_utimens,
	.setxattr	= mdb_sqlite_setxattr,
	.getxattr	= mdb_sqlite_getxattr,
	.listxattr	= mdb_sqlite_listxattr,
	.removexattr	= mdb_sqlite_removexattr,
#endif
};

static struct stackfs_metadb mdb_sqlite = {
	.name		= "sqlite",
	.ops		= &mdb_sqlite_ops,
	.ctx		= NULL,
	.priv		= NULL,
};

__attribute__((constructor)) static void mdb_sqlite_ctor(void)
{
	stackfs_register_metadb(&mdb_sqlite);
}

#if 0
__attribute__((destructor)) static void mdb_sqlite_dtor(void)
{
}
#endif

