-- 
-- metadb_sqlite.schema.sql
--

begin transaction;

-- remove any existing tables.
--drop table if exists stfs_super;
drop table if exists stfs_inode;
drop table if exists stfs_dirent;
drop table if exists stfs_symlink;

-- unix inode
create table stfs_inode (
	id	integer not null,	-- standard unix stat.
	dev	integer not null,
	mode	integer not null,
	nlink	integer not null,
	uid	integer not null,
	gid	integer not null,
	rdev	integer not null,
	size	integer not null,
	atime	integer not null,
	mtime	integer not null,
	ctime	integer not null,

	primary key (id asc)
);

-- directory entries
create table stfs_dirent (
	id	integer not null,
	d_ino	integer references stfs_inode(id),
	e_ino	integer references stfs_inode(id),
	name	text,

	primary key (id asc),
	unique (d_ino, name)
);

create index idx_e_ino on stfs_dirent (e_ino);

-- trigger for unlinking directory entries
create trigger tr_unlink update of nlink on stfs_inode
for each row when new.nlink <= 0
begin
	delete from stfs_dirent where e_ino = new.id;
	delete from stfs_inode where id = new.id;
end;

-- symbolic links
create table stfs_symlink (
	id	integer not null,
	ino	integer references stfs_inode(id),
	path	text,

	primary key (id asc)
);

-- insert root directory and dot entries.
insert into stfs_inode
	(dev, mode, nlink, uid, gid, rdev, size, atime, mtime, ctime)
values
	(0, 16832, 2, 0, 0, 0, 4096,
	strftime('%s', 'now'), strftime('%s', 'now'), strftime('%s', 'now'));

insert into stfs_dirent (d_ino, e_ino, name)
	values (1, 1, '.');
insert into stfs_dirent (d_ino, e_ino, name)
	values (1, 1, '..');

end transaction;

