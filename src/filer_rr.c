/* Copyright (C) 2013 Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * Refer to COPYING for the license of this program.
 * 
 */
#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "stackfs.h"

struct rr_ctx {
	int n_backends;
	char *backends[8];	/** TODO: change this to be dynamic */
};

static const char *configfile = "/tmp/stackfs/conf/filer_rr.conf";

static inline void get_rr_path(struct rr_ctx *self, uint64_t ino, char *buf)
{
	sprintf(buf, "%s/%02x/%016llx.sfs",
			self->backends[ino % self->n_backends],
			(uint8_t) (ino & 0xffUL), stackfs_llu(ino));
}

static int read_config(struct rr_ctx *self)
{
	FILE *fp;
	char *line;
	char pbuf[256];

	fp = fopen(configfile, "r");
	if (!fp)
		return -errno;

	while (fgets(pbuf, 255, fp) != NULL) {
		if (strempty(pbuf) || pbuf[0] == '#')
			continue;

		line = strtrim(pbuf);

		/** TODO: check NOMEM */
		self->backends[self->n_backends++] = strdup(line);
	}

	return self->backends ? 0 : -EINVAL;
}

static int rr_init(struct stackfs_ctx *ctx)
{
	int i, ret = 0;
	struct stackfs_filer *filer = ctx->filer;
	struct rr_ctx *self = calloc(1, sizeof(*self));
	uint8_t d;
	char pathbuf[PATH_MAX];

	if (!self)
		return -ENOMEM;

	ret = read_config(self);
	if (ret < 0)
		return ret;

	/**
	 * TODO:
	 * create subdirectories if not ready.
	 * currently, assume that all subdirectories are already created.
	 */
	for (i = 0; i < self->n_backends; i++)
		for (d = 0; d < 0xff; d++) {
			sprintf(pathbuf, "%s/%02x", self->backends[i], d);
			ret = mkdir(pathbuf, 0755);
			if (ret && errno != EEXIST)
				return -errno;
		}

	filer->ctx = ctx;
	filer->priv = self;

	return 0;
}

static void rr_exit(struct stackfs_ctx *ctx)
{
	int i;
	struct rr_ctx *self = stackfs_filer_private(ctx);

	if (!self)
		return;

	for (i = 0; i < self->n_backends; i++)
		if (self->backends[i])
			free(self->backends[i]);

	free(self);
}

static int rr_open(struct stackfs_ctx *ctx, uint64_t ino, int flags)
{
	char pathbuf[PATH_MAX];

	get_rr_path(stackfs_filer_private(ctx), ino, pathbuf);

	return open(pathbuf, flags);
}

static int rr_create(struct stackfs_ctx *ctx, uint64_t ino)
{
	FILE *fp;
	char pathbuf[PATH_MAX];

	get_rr_path(stackfs_filer_private(ctx), ino, pathbuf);

	fp = fopen(pathbuf, "w");
	if (!fp)
		return -errno;

	fclose(fp);
	return 0;
}

static int rr_truncate(struct stackfs_ctx *ctx, uint64_t ino, uint64_t newsize)
{
	char pathbuf[PATH_MAX];

	get_rr_path(stackfs_filer_private(ctx), ino, pathbuf);

	return truncate(pathbuf, (off_t) newsize);
}

static struct stackfs_filer_operations filer_rr_ops = {
	.init		= rr_init,
	.exit		= rr_exit,
	.open		= rr_open,
	.create		= rr_create,
	.truncate	= rr_truncate,
};

static struct stackfs_filer filer_rr = {
	.name		= "rr",
	.ops		= &filer_rr_ops,
	.ctx		= NULL,
	.priv		= NULL,
};

__attribute__((constructor)) static void filer_rr_ctor(void)
{
	stackfs_register_filer(&filer_rr);
}

#if 0
__attribute__((destructor)) static void filer_rr_dtor(void)
{
}
#endif

